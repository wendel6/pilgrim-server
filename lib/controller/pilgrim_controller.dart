import 'package:aqueduct/aqueduct.dart';
import 'package:pilgrim/pilgrim.dart';
import 'package:pilgrim/model/user.dart';

class PilgrimController extends ResourceController {
  PilgrimController(this.context);
  final ManagedContext context;

  @Operation.get()
  Future<Response> getAllUsers() async {
    final userQuery = Query<User>(context);
    final users = await userQuery.fetch();
    return Response.ok(users);
  }

  @Operation.get('id')
  Future<Response> getUserByID(@Bind.path('id') int id) async {
    final userQuery = Query<User>(context)..where((h) => h.id).equalTo(id);
    final user = await userQuery.fetchOne();

    if (user == null) {
      return Response.notFound();
    }

    return Response.ok(user);
  }

  @Operation.post()
  Future<Response> createUser() async {
    final user = User()..read(await request.body.decode(), ignore: ["id"]);
    final query = Query<User>(context)..values = user;
    final insertedUser = await query.insert();
    return Response.ok(insertedUser);
  }

  @Operation.put('id')
  Future<Response> updateUser(
      @Bind.path('id') int id, @Bind.body() User user) async {
    final query = Query<User>(context)
      ..where((u) => u.id).equalTo(id)
      ..values = user;

    return Response.ok(await query.updateOne());
  }
}
