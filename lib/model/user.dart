import 'package:pilgrim/pilgrim.dart';

class User extends ManagedObject<_User> implements _User {}

@Table(name: "test")
class _User {
  @Column(
    primaryKey: true,
    databaseType: ManagedPropertyType.integer,
    autoincrement: true,
  )
  int id;

  DateTime creation;
  String name;
  String email;
  String password_hash;
  String ip;
  Document device;
  DateTime moment;
  int x;
}
